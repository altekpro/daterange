<?php

namespace Altekpro\DateRange;

use DateTime;
use RuntimeException;

/**
 * Internal trait used by DateRange and DateTimeRange classes for definition of common functions.
 * Inheritance could not be used in this case due to annotation mapping used to enable these classes
 * as embeddable Doctrine entities.
 */
trait TDateRange
{
    /**
     * Creates a range with 1.1.XXXX and 31.12.XXXX as dates where XXXX is the given year.
     */
    public static function createForYear(int $year)
    {
        if ($year < 1970 || $year > 2100) {
            throw new \RuntimeException("Year out of bounds - should be between 1970 and 2100");
        }

        $range = new self(new DateTime($year . '-01-01 00:00:00'), new DateTime($year . '-12-31 23:59:59'));

        return $range;
    }



    /**
     * Creates a range with XXXX-NN-01 and XXXX-MM-YY as dates where:
     * XXXX is the given year
     * N is the first month of the quarter the key date belongs to (e.g. 4 if the key date is 2025-05-12)
     * M is the last month of the quarter (e.g. 6 in the above example)
     * Y is the number of days in the given month
     */
    public static function createForQuarter(DateTime $key_date)
    {
        $year = (int)$key_date->format('Y');
        $month = (int)$key_date->format('n');
        $end_month = (int)(ceil($month / 3) * 3);
        $start_month = $end_month - 2;
        $end_date = new DateTime("$year-$end_month-01 00:00:00");
        $end_date->modify('last day of this month');
        return new DateRange("$year-$start_month-01 00:00:00", $end_date);
    }



    /**
     * Returns all ranges within the main interval, either as new DateRange objects, or as DateTime
     *
     * @return DateRange[]|DateTime[]
     */
    public function getDayRanges($as_range = true)
    {
        $result = [];
        $date = clone $this->from;
        while($date <= $this->to) {
            $result[] = $as_range ? new self(clone $date, clone $date) : clone $date;
            $date->modify('+1 day');
        }
        return $result;
    }



    /**
     * Return new ranges that represent individual quarters contained within
     * the original range. Result includes partial quarters, which have only
     * one end aligned to te quarter start / end. Result can also include
     * single quarter with both start / end not aligned with actual quarter,
     * in case the range is smaller than single quarter
     *
     * If used with DateTimeRange, preserves time information at the start / end of the range
     *
     * @return DateRange[]|DateTimeRange[] Depending on the object in use
     */
    public function getQuarters() : array
    {
        // define starts of each quarter
        $quarters = [
            1 => 'first day of january',
            2 => 'first day of april',
            3 => 'first day of july',
            4 => 'first day of october'
        ];

        // result carries all resulting ranges, pivot date is used for iteration
        $result = [];
        $pivot = clone $this->getFrom();

        // iterate from the start of this range, until we hit its end
        while($pivot <= $this->getTo()) {

            // find this quarter start string based on current month of the pivot date
            $month = $pivot->format('n');
            $key = 1;
            while($month >= ($key - 1) * 3 + 1) {
                $quarter = $quarters[$key];
                $key++;
            }

            // create quarter start date from the quarter string
            $quarter_start = new DateTime("$quarter {$pivot->format('Y')}");
            $quarter_start->setTime(0, 0, 0);

            // quarter end date is taken from the next entry in the quarters, or the first entry (if we are in the last quarter) next year
            $year = $pivot->format('Y');
            $next_quarter = isset($quarters[$key]) ? "{$quarters[$key]} $year" : "{$quarters[1]} " . $year + 1;
            $quarter_end = new DateTime($next_quarter);
            $quarter_end->modify('-1 day');
            $quarter_end->setTime(23, 59, 59);

            // create full quarter range and intersect it with this range, creating a result
            // that is at most the full quarter range, but can be smaller if current range starts / ends in the middle of the quarter
            $full_quarter = new self($quarter_start, $quarter_end);
            $aligned_range = $full_quarter->intersection($this);

            // save to the result and advance to pivot date to start of next day
            $result[] = $aligned_range;
            $pivot = clone $aligned_range->getTo();
            $pivot->setTime(23, 59, 59);
            $pivot->modify('+1 second');
        }

        return $result;
    }



    /**
     * Return new ranges that represent individual months contained within
     * the original range. Result includes partial months, which have only
     * one end aligned to te month start / end. Result can also include
     * single month with both start / end not aligned with actual month,
     * in case the range is smaller than single month
     *
     * If used with DateTimeRange, preserves time information at the start / end of the range
     *
     * @return DateRange[]|DateTimeRange[] Depending on the object in use
     */
    public function getMonths() : array
    {
        // result carries all resulting ranges, pivot date is used for iteration
        $result = [];
        $pivot = clone $this->getFrom();

        // iterate from the start of this range, until we hit its end
        while($pivot <= $this->getTo()) {
            $start = clone $pivot;
            $start->modify('first day of this month');
            $end = clone $pivot;
            $end->modify('last day of this month');
            $full_month = new DateRange($start, $end);
            $aligned_range = $full_month->intersection($this);

            // save to the result and advance to pivot date to start of next day
            $result[] = $aligned_range;
            $pivot = clone $aligned_range->getTo();
            $pivot->setTime(23, 59, 59);
            $pivot->modify('+1 second');
        }

        return $result;
    }



    /**
     * Return new ranges that represent individual months contained within
     * the original range. Result includes partial months, which have only
     * one end aligned to te month start / end. Result can also include
     * single month with both start / end not aligned with actual month,
     * in case the range is smaller than single month
     *
     * If used with DateTimeRange, preserves time information at the start / end of the range
     *
     * @return DateRange[]|DateTimeRange[] Depending on the object in use
     */
    public function getHalfYears() : array
    {
        // result carries all resulting ranges, pivot date is used for iteration
        $result = [];
        $pivot = clone $this->getFrom();

        // iterate from the start of this range, until we hit its end
        while($pivot <= $this->getTo()) {
            $year = $pivot->format('Y');

            $cutoff = new DateTime("1st June $year 00:00:00");
            $start = $cutoff > $pivot ? new DateTime("1st January $year 00:00:00") : $cutoff;
            $end = $cutoff > $pivot ? new DateTime("31 May $year 23:59:59") : new DateTime("31 December $year 23:59:59");

            $full_halfyear = new DateRange($start, $end);
            $aligned_range = $full_halfyear->intersection($this);

            // save to the result and advance to pivot date to start of next day
            $result[] = $aligned_range;
            $pivot = clone $aligned_range->getTo();
            $pivot->setTime(23, 59, 59);
            $pivot->modify('+1 second');
        }

        return $result;
    }



    /**
     * Return new ranges that represent individual years contained within
     * the original range. Result includes partial years, which have only
     * one end aligned to te year start / end. Result can also include
     * single year with both start / end not aligned with actual year,
     * in case the range is smaller than single year
     *
     * If used with DateTimeRange, preserves time information at the start / end of the range
     *
     * @return DateRange[]|DateTimeRange[] Depending on the object in use
     */
    public function getYears() : array
    {
        // result carries all resulting ranges, pivot date is used for iteration
        $result = [];
        $pivot = clone $this->getFrom();

        // iterate from the start of this range, until we hit its end
        while($pivot <= $this->getTo()) {
            $start = clone $pivot;
            $start->modify('first day of January');
            $end = clone $pivot;
            $end->modify('last day of December');

            $full_year = new DateRange($start, $end);
            $aligned_range = $full_year->intersection($this);

            // save to the result and advance to pivot date to start of next day
            $result[] = $aligned_range;
            $pivot = clone $aligned_range->getTo();
            $pivot->setTime(23, 59, 59);
            $pivot->modify('+1 second');
        }

        return $result;
    }



    /**
     * Formats the range objects as string, passing the format argument to both DateTime objects
     * and joining them via separator. If single day mode is true and the range is within the same
     * day, applies the format to just the first date end returns it.
     */
    public function format(string $format, $single_day_mode = false, string $separator = ' - ') : string
    {
        if ($single_day_mode && $this->from->format('Y-m-d') == $this->to->format('Y-m-d')) {
            return $this->from->format($format);
        }

        return $this->from->format($format) . $separator . $this->to->format($format);
    }



    /**
     * Returns the year in which this range resides or throws an exception if it spans more than
     * one year.
     *
     * @throws RuntimeException
     */
    public function getYear() : int
    {
        if ($this->from->format('Y') != $this->to->format('Y')) {
            throw new \RuntimeException('This range spans multiple years.');
        }

        return $this->from->format('Y');
    }



    /**
     * Returns true if date_to of the range is in the past (meaning entire range is in the past)
     */
    public function isInPast() : bool
    {
        $now = (new \DateTime);
        return $this->to < $now;
    }



    /**
     * Returns true if the given range is partially inside this range
     */
    public function overlapsRange(IDateRange $range) : bool
    {
        return $this->from <= $range->getTo() && $this->to >= $range->getFrom();
    }



    /**
     * Returns one to three date ranges in an array, representing all ranges created
     * by 'sandwiching' another range into the current one.
     *
     * @return DateRange[]|DateTimeRange[]
     */
    public function cutWithRange(IDateRange $range) : array
    {
        if (!$this->overlapsRange($range)) {
            return [$this];
        }

        if ($range->getFrom() > $this->getFrom() && $range->getTo() < $this->getTo()) {
            $left_from = $this->from;
            $left_to = clone $range->getFrom();
            $left_to->modify('-1 second');

            $middle_from = $range->getFrom();
            $middle_to = clone $range->getTo();

            $right_from = $range->getTo();
            $right_from->modify('+1 second');
            $right_to = $this->to;

            return [new self($left_from, $left_to), new self($middle_from, $middle_to), new self($right_from, $right_to)];
        } elseif ($range->getFrom() > $this->getFrom()) {
            $left_from = $this->from;
            $left_to = clone $range->getFrom();
            $left_to->modify('-1 second');

            $right_from = $range->getFrom();
            $right_to = $this->to;

            return [new self($left_from, $left_to), new self($right_from, $right_to)];
        } elseif ($range->getTo() < $this->getTo()) {
            $left_from = $this->from;
            $left_to = clone $range->getTo();

            $right_from = $range->getTo();
            $right_from->modify('+1 second');
            $right_to = $this->to;

            return [new self($left_from, $left_to), new self($right_from, $right_to)];
        }
    }



    public function intersection(IDateRange $range) : ?IDateRange
    {
        if (!$this->overlapsRange($range)) {
            return null;
        }

        return new self(
            $this->getFrom() < $range->getFrom() ?  clone $range->getFrom() : clone $this->getFrom(),
            $this->getTo() > $range->getTo() ? clone $range->getTo() : clone $this->getTo()
        );
    }



    public function substract(IDateRange $range) : array
    {
        if ($range->getTo() <= $this->getFrom() || $range->getFrom() >= $this->getTo()) {
            return [$this];
        }

        if ($range->getFrom() <= $this->getFrom() && $range->getTo() >= $this->getTo()) {
            return [];
        }

        $to = clone $range->getTo();
        $to->modify('+1 second');
        if ($range->getFrom() <= $this->getFrom()) {
            return [new self($to, $this->getTo())];
        }

        $from = clone $range->getFrom();
        $from->modify('-1 second');
        if ($range->getTo() >= $this->getTo()) {
            return [new self($this->getFrom(), $from)];
        }

        return [
            new self($this->getFrom(), $from),
            new DateRange($to, $this->getTo())
        ];
    }



    public static function createNewRangesFromTwoRangeArrays(array $ranges_1, array $ranges_2) : array
    {
        self::sortArrayOfRanges($ranges_1);
        self::sortArrayOfRanges($ranges_2);

        $result = [];
        foreach ($ranges_1 as $range_1) {
            if (!($range_1 instanceof self)) {
                throw new RuntimeException('$ranges_1 expects only ' . get_class(self) . ' objects, "' . get_class($range_1) . '" found.');
            }

            $subresult = [$range_1];
            foreach ($ranges_2 as $range_2) {
                if (!($range_2 instanceof self)) {
                    throw new RuntimeException('$ranges_2 expects only ' . get_class(self) . ' objects, "' . get_class($range_2) . '" found.');
                }

                foreach ($subresult as $index => $range_3) {
                    if ($range_3->overlapsRange($range_2)) {
                        unset($subresult[$index]);
                        $subresult = array_merge($subresult, $range_3->cutWithRange($range_2));
                    }
                }
            }

            $result = array_merge($result, $subresult);
        }

        return $result;
    }



    public static function sortArrayOfRanges(array &$ranges) : void
    {
        usort($ranges, function(IDateRange $a, IDateRange $b) {
            return strcmp($a->format('YmdHisu', ''), $b->format('YmdHisu', ''));
        });
    }



    public function getFrom($format = null)
    {
        if ($format) {
            return $this->from->format($format);
        }

        return $this->from;
    }

    public function setFrom(DateTime $from)
    {
        $this->from = $from;
    }

    public function getTo($format = null)
    {
        if ($format) {
            return $this->to->format($format);
        }

        return $this->to;
    }

    public function setTo(DateTime $to)
    {
        $this->to = $to;
    }

    public function modifyBoth(string $by) : void
    {
        $this->from->modify($by);
        $this->to->modify($by);
    }
}
