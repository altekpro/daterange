<?php

namespace Altekpro\DateRange;

interface IClosedIntervalDateRange extends IDateRange
{
    /**
     * Formats the range objects as string, passing the format argument to both DateTime objects
     * and joining them via separator. If single day mode is true and the range is within the same
     * day, applies the format to just the first date end returns it.
     */
    public function format(string $format, $single_day_mode = false, string $separator = '');

    /**
     * Returns all ranges within the main interval, either as new DateRange objects, or as DateTime
     *
     * @return DateRange[]|DateTime[]
     */
    public function getDayRanges($as_range);
}