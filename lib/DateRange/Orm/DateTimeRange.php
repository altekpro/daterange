<?php

namespace Altekpro\DateRange\Orm;

use Altekpro\DateRange\DateTimeRange as _DateTimeRange;
use Doctrine\ORM\Mapping as ORM;

/**
 * @see Altekpro\DateRange\Orm\DateRange
 *
 * @ORM\Embeddable
 *
 * @author Jan Pavlíček <jan@pavlicek.dev>
 * @since 1.0.0
 */
#[ORM\Embeddable]
class DateTimeRange extends _DateTimeRange
{
    /**
     * @ORM\Column(type="datetime")
     */
    #[ORM\Column(type: 'datetime')]
    protected $from;


    /**
     * @ORM\Column(type="datetime")
     */
    #[ORM\Column(type: 'datetime')]
    protected $to;
}

