<?php

namespace Altekpro\DateRange\Orm;

use Altekpro\DateRange\MaxMinDateRange as _MaxMinDateRange;
use Doctrine\ORM\Mapping as ORM;

/**
 * This class can be used with Doctrine ORM to save date range object into database
 * using @Embed annotation. It is implemented as simple proxy to avoid having Doctrine
 * dependency on the base DateRange objects.
 *
 * @ORM\Embeddable
 * @ORM\HasLifecycleCallbacks
 *
 * @author Josef Šíma <josef.sima@asua.dev>
 * @since 1.0.0
 */

#[
    ORM\Embeddable,
    ORM\HasLifecycleCallbacks
]
class MaxMinDateRange extends _MaxMinDateRange
{
    /**
     * @ORM\Column(type="date")
     */
    #[ORM\Column(type: 'date')]
    protected $from;


    /**
     * @ORM\Column(type="date")
     */
    #[ORM\Column(type: 'date')]
    protected $to;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    #[
        ORM\PrePersist,
        ORM\PreUpdate
    ]
    public function prePersist() {
        if($this->from === null) {
            $this->from = $this->getMinDate();
        }
        if($this->to === null) {
            $this->to = $this->getMaxDate();
        }
    }


    /**
     * @ORM\PostLoad()
     * @throws \Exception
     */
    #[ORM\PostLoad]
    public function postLoad() {

        if($this->toDateTimeMin($this->from) == $this->getMinDate()) {
            $this->from = null;
        }
        if($this->toDateTimeMax($this->to) == $this->getMaxDate()) {
            $this->to = null;
        }
    }
}