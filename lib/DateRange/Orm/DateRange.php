<?php

namespace Altekpro\DateRange\Orm;

use Altekpro\DateRange\DateRange as _DateRange;
use Doctrine\ORM\Mapping as ORM;

/**
 * This class can be used with Doctrine ORM to save date range object into database
 * using @Embed annotation. It is implemented as simple proxy to avoid having Doctrine
 * dependency on the base DateRange objects.
 *
 * @ORM\Embeddable
 *
 * @author Jan Pavlíček <jan@pavlicek.dev>
 * @since 1.0.0
 */
#[ORM\Embeddable]
class DateRange extends _DateRange
{
    /**
     * @ORM\Column(type="date")
     */
    #[ORM\Column(type: 'date')]
    protected $from;


    /**
     * @ORM\Column(type="date")
     */
    #[ORM\Column(type: 'date')]
    protected $to;
}
