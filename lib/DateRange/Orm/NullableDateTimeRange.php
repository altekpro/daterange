<?php

namespace Altekpro\DateRange\Orm;

use Altekpro\DateRange\DateTimeRange as _DateTimeRange;
use Doctrine\ORM\Mapping as ORM;

/**
 * @see Altekpro\DateRange\Orm\DateRange
 *
 * @ORM\Embeddable
 *
 * @author Jan Pavlíček <jan@pavlicek.dev>
 * @since 1.0.0
 */
#[ORM\Embeddable]
class NullableDateTimeRange extends _DateTimeRange
{
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected $from;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected $to;
}

