<?php

namespace Altekpro\DateRange;

use DateTime;

/**
 * Same object as DateRange but used for doctrine when time should also be persisted.
 *
 * @see Altekpro\DateRange
 * @ORM\Embeddable
 */
class DateTimeRange implements IClosedIntervalDateRange
{
    use TDateRange;


    protected DateTime $from;


    protected DateTime $to;


    public function __construct($from, $to)
    {
        if (!($from instanceof DateTime)) {
            $from = new DateTime($from);
            $to = new DateTime($to);
        }

        if (!$from || !$to) {
            throw new \InvalidArgumentException("Both date boundaries must be passed, either as " .
                "DateTime objects, or DateTime::__construct() compatible strings.");
        }

        if ($to < $from) {
            throw new \InvalidArgumentException("Date from cannot be in the future of date to.");
        }

        $this->from = $from;
        $this->to = $to;
    }



    /**
     * Returns true if this range is fully enclosed within a given range (closed interval)
     */
    public function isInRange($range) : bool
    {
        if (!($range instanceof DateRange) && !($range instanceof DateTimeRange)) {
            throw new \InvalidArgumentException("Argument must be either " . self::class . " or " . DateTimeRange::class . " object.");
        }

        $from = $range->getFrom();
        $to = $range->getTo();

        if ($range instanceof DateRange) {
            $from = (clone $from)->setTime(0, 0, 0);
            $to = (clone $to)->setTime(23, 59, 59);
        }

        if ($this->from >= $from && $this->to <= $to) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Returns true if current date falls within this range (closed interval)
     */
    public function isCurrent(DateTime $now = null) : bool
    {
        $now = $now ?: new \DateTime;
        return $now >= $this->from && $now <= $this->to;
    }
}
