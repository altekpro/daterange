<?php

namespace Altekpro\DateRange;

use DateTime;

/**
 * Simple class holding tow dates used to abstract work with date intervals. Provides several
 * utility methods to initialize or reduce and getters to retrieve individual dates.
 *
 * Methods are defined in the internal trait TDateRange. This was done to be able to define
 * DateRange and DateTimeRange separately for Doctrine column definitions.
 *
 * This class allow to create open interval with use null as "from" and/or "to" date.
 * To database is stored PHP max/min date time when is given null. For store null instead of
 * max/min dateTime use NullableDateRange
 *
 * IMPORTANT - don't forget define global constants DOCTRINE_MIN_DATETIME and DOCTRINE_MAX_DATETIME to minimal and maximal
 * date time value, witch is supported by your database (or other your min/max date time value). If constants not defined
 * then date_time created from PHP_INT_MAX and PHP_INT_MIN will be used
 * (but this values is not supported for most of a databases)
 *
 * @ORM\Embeddable
 * @ORM\HasLifecycleCallbacks
 *
 * @author Jan Pavlíček <josef.sima@asua.dev>, Jan Pavlíček <jan@pavlicek.dev>
 * @since 1.0.0
 */
class MaxMinDateRange extends NullableDateRange
{
}