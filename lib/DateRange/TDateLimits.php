<?php

namespace Altekpro\DateRange;

trait TDateLimits
{
    /**
     * Set maximal time to given instance of DateTime and return, or if is given null return maximal possible DateTime
     * @param $date
     * @return \DateTime
     * @throws \Exception
     */
    protected function toDateTimeMax($date)
    {
        if($date instanceof \DateTime) {
            return (clone $date)->setTime(23, 59, 59);
        }else {
            return $this->getMaxDate();
        }
    }

    /**
     * Set minimal time to given instance of DateTime and return, or if is given null return minimal possible DateTime
     * @param $date
     * @return \DateTime
     * @throws \Exception
     */
    protected function toDateTimeMin($date) {
        if($date instanceof \DateTime) {
            return (clone $date)->setTime(0, 0, 0);
        }else {
            return $this->getMinDate();
        }
    }

    protected function getMinDate()
    {
        if(defined('DOCTRINE_MIN_DATETIME')) {
            return new \DateTime(DOCTRINE_MIN_DATETIME);
        }
        return new \DateTime(date('Y-m-d H:i:s', PHP_INT_MIN));
    }

    protected function getMaxDate()
    {
        if(defined('DOCTRINE_MAX_DATETIME')) {
            return new \DateTime(DOCTRINE_MAX_DATETIME);
        }
        return new \DateTime(date('Y-m-d H:i:s', PHP_INT_MAX));
    }
}