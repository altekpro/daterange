<?php

namespace Altekpro\DateRange;

interface IDateRange
{

    /**
     * Returns true if this range or datetime is fully enclosed within a given range (closed interval)
     */
    public function isInRange($range);

    /**
     * Returns true if current date falls within this range (closed interval)
     */
    public function isCurrent() : bool;

    /**
     * Creates a range with 1.1.XXXX and 31.12.XXXX as dates where XXXX is the given year.
     */
    public static function createForYear(int $year);

    /**
     * Returns the year in which this range resides or throws an exception if it spans more than
     * one year.
     *
     * @throws RuntimeException
     */
    public function getYear() : int;

    /**
     * Returns true if date_to of the range is in the past (meaning entire range is in the past)
     */
    public function isInPast() : bool;

    /**
     * Returns true if the given range is partially inside this range
     */
    public function overlapsRange(IDateRange $range) : bool;

    public function getFrom($format = null);

    public function getTo($format = null);

    public function setFrom(\DateTime $from);

    public function setTo(\DateTime $to);

    public function modifyBoth(string $by) : void;

}
