<?php

namespace Altekpro\DateRange;
use \DateTime;

/**
 * Simple class holding tow dates used to abstract work with date intervals. Provides several
 * utility methods to initialize or reduce and getters to retrieve individual dates.
 *
 * Methods are defined in the internal trait TDateRange. This was done to be able to define
 * DateRange and DateTimeRange separately for Doctrine column definitions.
 *
 * This class allow to create open interval with use null as "from" and/or "to" date.
 * To database is stored given DateTime or null value. For store max/min dateTime instead of null use MaxMinDateRange
 *
 * @ORM\Embeddable
 *
 * @author Josef Šíma <josef.sima@asua.dev>, Jan Pavlíček <jan@pavlicek.dev>
 * @since 1.0.0
 * @deprecated Since TDateRange was and will be further extended, this class should be considered deprecated. It will probably be re-implemented as
 *             a more robust solution capable of utilizing TDateRange trait in the future.
 */
class NullableDateRange implements IOpenIntervalDateRange
{
    use TNullableDateRange;
    use TDateLimits;

    protected $from;

    protected $to;

    public function __construct($from, $to)
    {
        try {
            if($from !== null && !($from instanceof DateTime)) {
                $from = new DateTime($from);
            }

            if($to !== null && !($to instanceof DateTime)) {
                $to = new DateTime($to);
            }
        } catch (\Throwable $e) {
            throw new \InvalidArgumentException("Both date boundaries must be passed, either as " .
                "DateTime objects, null ,or DateTime::__construct() compatible strings.");
        }


        $_from = $this->toDateTimeMin($from);
        $_to = $this->toDateTimeMax($to);

        if ($_to < $_from) {
            throw new \InvalidArgumentException("Date from cannot be in the future of date to.");
        }

        if ($from instanceof DateTime) {
            $from = (clone $from)->setTime(0, 0, 0);
        }

        if($to instanceof DateTime) {
            $to = (clone $to)->setTime(23, 59, 59);
        }

        $this->from = $from;
        $this->to = $to;
    }

    /**
     * Returns true if this range or datetime is fully enclosed within a given range (closed interval)
     */
    public function isInRange($range)
    {
        if (!$range instanceof IDateRange) {
            throw new \InvalidArgumentException("Argument must be either " . self::class . " or " . DateTimeRange::class . " or " . DateTime::class . "  object.");
        }

        $_from = $this->toDateTimeMin($this->from);
        $_to = $this->toDateTimeMax($this->to);


        if ($_from >= $this->toDateTimeMin($range->getFrom()) && $_to <= $this->toDateTimeMax($range->getTo())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns true if current date falls within this range (closed interval)
     */
    public function isCurrent(): bool
    {
        $_from = $this->toDateTimeMin($this->from);
        $_to = $this->toDateTimeMax($this->to);

        $now = (new \DateTime)->setTime(0, 0, 0);
        return $now >= $_from && $now <= $_to;
    }

}
