<?php

namespace Altekpro\DateRange;

use DateTime;

/**
 * Simple class holding tow dates used to abstract work with date intervals. Provides several
 * utility methods to initialize or reduce and getters to retrieve individual dates.
 *
 * Methods are defined in the internal trait TDateRange. This was done to be able to define
 * DateRange and DateTimeRange separately for Doctrine column definitions.
 *
 * @ORM\Embeddable
 *
 * @author Jan Pavlíček <jan@pavlicek.dev>
 * @since 1.0.0
 */
class DateRange implements IClosedIntervalDateRange
{
    use TDateRange;
    use TDateLimits;

    protected $from;


    protected $to;


    public function __construct($from, $to)
    {
        if (!($from instanceof DateTime)) {
            $from = new DateTime($from);
        }

        if (!($to instanceof DateTime)) {
            $to = new DateTime($to);
        }

        if (!$from || !$to) {
            throw new \InvalidArgumentException("Both date boundaries must be passed, either as " .
                "DateTime objects, or DateTime::__construct() compatible strings.");
        }

        $from = (clone $from)->setTime(0, 0, 0);
        $to = (clone $to)->setTime(23, 59, 59);

        if ($to < $from) {
            throw new \InvalidArgumentException("Date from cannot be in the future of date to.");
        }

        $this->from = $from;
        $this->to = $to;
    }


    /**
     * Returns true if this range or datetime is fully enclosed within a given range (closed interval)
     */
    public function isInRange($range) : bool
    {
        if (!($range instanceof IDateRange)) {
            throw new \InvalidArgumentException("Argument must instance of ". IDateRange::class ."  object.");
        }


        if ($this->from >= $this->toDateTimeMin($range->getFrom()) && $this->to <= $this->toDateTimeMax($range->getTo())) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Returns true if current date falls within this range (closed interval)
     */
    public function isCurrent() : bool
    {
        $now = (new \DateTime)->setTime(0, 0, 0);
        return $now >= $this->from && $now <= $this->to;
    }
}
