<?php

namespace Altekpro\DateRange\Odm;

use Altekpro\DateRange\DateTimeRange as _DateTimeRange;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @see Altekpro\DateRange\Odm\DateRange
 *
 * @ODM\EmbeddedDocument
 *
 * @author Jan Pavlíček <jan@pavlicek.dev>
 * @since 1.0.0
 */
class DateTimeRange extends _DateTimeRange
{
    /**
     * @ODM\Field(type="date")
     */
    protected $from;


    /**
     * @ODM\Field(type="date")
     */
    protected $to;
}
