<?php

namespace Altekpro\DateRange\Odm;

use Altekpro\DateRange\DateRange as _DateRange;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * This class can be used with Doctrine ODM to save date range object into database
 * using @EmbedOne annotation. It is implemented as simple proxy to avoid having Doctrine
 * dependency on the base DateRange objects.
 *
 * @ODM\EmbeddedDocument
 *
 * @author Jan Pavlíček <jan@pavlicek.dev>
 * @since 1.0.0
 */
class DateRange extends _DateRange
{
    /**
     * @ODM\Field(type="date")
     */
    protected $from;


    /**
     * @ODM\Field(type="date")
     */
    protected $to;
}
