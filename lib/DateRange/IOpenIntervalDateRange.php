<?php

namespace Altekpro\DateRange;

interface IOpenIntervalDateRange extends IDateRange
{
    public function setFrom(\DateTime|null $from);

    public function setTo(\DateTime|null $to);

    public function isNull() : bool;
}