<?php

namespace Altekpro\DateRange;

use DateTime;

/**
 * Internal trait used by DateRange and DateTimeRange classes for definition of common functions.
 * Inheritance could not be used in this case due to annotation mapping used to enable these classes
 * as embeddable Doctrine entities.
 */
trait TNullableDateRange
{
    /**
     * Creates a range with 1.1.XXXX and 31.12.XXXX as dates where XXXX is the given year.
     */
    public static function createForYear(int $year)
    {
        if ($year < 1970 || $year > 2100) {
            throw new \RuntimeException("Year out of bounds - should be between 1970 and 2100");
        }

        $range = new self(new DateTime($year . '-01-01 00:00:00'), new DateTime($year . '-12-31 23:59:59'));

        return $range;
    }


    /**
     * Returns the year in which this range resides or throws an exception if it spans more than
     * one year.
     *
     * @throws RuntimeException
     */
    public function getYear() : int
    {
        if ($this->toDateTimeMin($this->from)->format('Y') != $this->toDateTimeMax($this->to)->format('Y')) {
            throw new \RuntimeException('This range spans multiple years.');
        }

        return $this->from->format('Y');
    }



    /**
     * Returns true if date_to of the range is in the past (meaning entire range is in the past)
     */
    public function isInPast() : bool
    {
        $now = (new \DateTime);
        return $this->toDatetimeMax($this->to) < $now;
    }



    /**
     * Returns true if the given range is partially inside this range
     */
    public function overlapsRange($range) : bool
    {
        return  $this->toDatetimeMin($this->from) <= $this->toDatetimeMax($range->getTo()) &&
                        $this->toDatetimeMax($this->to) >= $this->toDatetimeMin($range->getFrom());
    }



    public function getFrom($format = null)
    {

        if ($format) {
            return $this->from?->format($format);
        }

        return $this->from;
    }

    public function setFrom(DateTime|null $from)
    {
        $this->from = $from;
    }

    public function getTo($format = null)
    {
        if ($format) {
            return $this->to?->format($format);
        }

        return $this->to;
    }

    public function setTo(DateTime|null $to)
    {
        $this->to = $to;
    }

    public function modifyBoth(string $by) : void
    {
        if($this->from !== null) {
            $this->from->modify($by);
        }

        if($this->to !== null) {
            $this->to->modify($by);
        }
    }


    public function isNull() : bool
    {
        return $this->from === null && $this->to === null;
    }
}
