DateRange
==============

Summary
-------

Pair of classes used to simplify working with date ranges and intervals. Provides containers for
starting and ending dates and several methods to get information about the range, find related
dates, format the range etc.

Installation
------------

First, add this repository as custom composer repository in composer.json. Than add the library itself:

```json
{
    "altekpro/daterange": "dev-main"
}
```


Basic usage
-----------

Library provides two classes to use - DateRange and DateTimeRange. If no time information is required,
use the first one - it sets the time of starting and ending dates to 00:00:00 and 23:59:59 respectively, if the dates
are passed via constructor. It clones the dates, so it does not mutate the original objects.

List of functions will be added when stable version is released.

### Example:
```php
$from = new DateTime('2021-01-05');
$to = new DateTime('2021-01-12');
$range = new \Altekpro\DateRange\DateRange($from, $to);

$range->modifyBoth('+5 days');

echo $range->getFrom()->format('Y-m-d');
// 2021-01-10
```

Using with doctrine
-------------------

Library provides extended classes for usage with Doctrine ORM and ODM. These classes are simple proxies
for the original DateRange classes that add Doctrine specific annotations. They can be found in the ORM and
ODM namepsaces. Both use embedding functionality of the respective doctrine library to allow seamless
usage in entities and documents.

For ODM:
https://www.doctrine-project.org/projects/doctrine-mongodb-odm/en/2.2/reference/embedded-mapping.html

For ORM:
https://www.doctrine-project.org/projects/doctrine-orm/en/2.9/tutorials/embeddables.html
