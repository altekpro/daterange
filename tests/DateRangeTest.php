<?php

namespace Altekpro\DateRange;

use PHPUnit\Framework\TestCase;
use DateTime;

class DateRangeTest extends TestCase
{
    public function testCannotCreateForBackwardsDates()
    {
        $from = new DateTime('2020-06-15');
        $to = new DateTime('2020-06-14');

        $this->expectException(\InvalidArgumentException::class);

        new DateRange($from, $to);
    }



    public function testCreateForYear()
    {
        $range = DateRange::createForYear(2020);
        $this->assertEquals(['2020-01-01', '2020-12-31'], [$range->getFrom()->format('Y-m-d'), $range->getTo()->format('Y-m-d')]);

        $range = DateRange::createForYear(2022);
        $this->assertEquals(['2022-01-01', '2022-12-31'], [$range->getFrom()->format('Y-m-d'), $range->getTo()->format('Y-m-d')]);

        $this->expectException(\RuntimeException::class);
        $range = DateRange::createForYear(45);

        $this->expectException(\RuntimeException::class);
        $range = DateRange::createForYear(250);
    }



    public function testGetDayRangesAsRanges()
    {
        $range = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-01-03'));
        $ranges = $range->getDayRanges();

        $this->assertEquals(['2020-01-01', '2020-01-01'], [$ranges[0]->getFrom()->format('Y-m-d'), $ranges[0]->getTo()->format('Y-m-d')]);
        $this->assertEquals(['2020-01-02', '2020-01-02'], [$ranges[1]->getFrom()->format('Y-m-d'), $ranges[1]->getTo()->format('Y-m-d')]);
        $this->assertEquals(['2020-01-03', '2020-01-03'], [$ranges[2]->getFrom()->format('Y-m-d'), $ranges[2]->getTo()->format('Y-m-d')]);
    }



    public function testGetDayRangesAsDates()
    {
        $range = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-01-03'));
        $ranges = $range->getDayRanges(false);

        $this->assertEquals('2020-01-01', $ranges[0]->format('Y-m-d'));
        $this->assertEquals('2020-01-02', $ranges[1]->format('Y-m-d'));
        $this->assertEquals('2020-01-03', $ranges[2]->format('Y-m-d'));
    }



    public function testFormat()
    {
        $range = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-01-03'));
        $this->assertEquals('2020-01-01 - 2020-01-03', $range->format('Y-m-d'));
        $this->assertEquals('1.1.2020 / 3.1.2020', $range->format('j.n.Y', false, ' / '));


        $range = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-01-01'));
        $this->assertEquals('2020-01-01 - 2020-01-01', $range->format('Y-m-d'));
        $this->assertEquals('2020-01-01', $range->format('Y-m-d', true));
    }



    public function testYearConversion()
    {
        $range = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-12-31'));
        $this->assertEquals(2020, $range->getYear());

        $range = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-06-31'));
        $this->assertEquals(2020, $range->getYear());

        $this->expectException(\RuntimeException::class);
        $range = new DateRange(new DateTime('2019-06-01'), new DateTime('2020-06-31'));
        $range->getYear();
    }



    public function testRangeIsInRange()
    {
        $outer = new DateRange(new DateTime('2020-01-15'), new DateTime('2020-01-27'));

        $inner = new DateRange(new DateTime('2020-01-15'), new DateTime('2020-01-27'));
        $this->assertTrue($inner->isInRange($outer));

        $inner = new DateRange(new DateTime('2020-01-18'), new DateTime('2020-01-27'));
        $this->assertTrue($inner->isInRange($outer));

        $inner = new DateRange(new DateTime('2020-01-15'), new DateTime('2020-01-20'));
        $this->assertTrue($inner->isInRange($outer));

        $inner = new DateRange(new DateTime('2020-01-18'), new DateTime('2020-01-20'));
        $this->assertTrue($inner->isInRange($outer));

        $inner = new DateRange(new DateTime('2020-01-14'), new DateTime('2020-01-27'));
        $this->assertFalse($inner->isInRange($outer));

        $inner = new DateRange(new DateTime('2020-01-15'), new DateTime('2020-01-28'));
        $this->assertFalse($inner->isInRange($outer));

        $inner = new DateRange(new DateTime('2020-01-14'), new DateTime('2020-01-28'));
        $this->assertFalse($inner->isInRange($outer));

        $inner = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-01-02'));
        $this->assertFalse($inner->isInRange($outer));

        $this->expectException(\InvalidArgumentException::class);
        $inner = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-01-02'));
        $inner->isInRange(new \StdClass);
    }



    public function testTodayIsInsideRange()
    {
        $range = new DateRange((new DateTime())->modify('-1 day'), (new DateTime())->modify('+1 day'));
        $this->assertTrue($range->isCurrent());

        $range = new DateRange(new DateTime(), new DateTime());
        $this->assertTrue($range->isCurrent());

        $range = new DateRange(new DateTime('1999-01-01'), new DateTime('1999-01-02'));
        $this->assertFalse($range->isCurrent());
    }



    public function testIsInPast()
    {
        $range = new DateRange(new DateTime('2020-06-01'), new DateTime('2020-06-15'));
        $this->assertTrue($range->isInPast());

        $range = new DateRange((new DateTime())->modify('-1 day'), new DateTime());
        $this->assertFalse($range->isInPast());

        $range = new DateRange(new DateTime(), (new DateTime())->modify('+1 day'));
        $this->assertFalse($range->isInPast());

        $range = new DateRange((new DateTime())->modify('+2 days'), (new DateTime())->modify('+3 days'));
        $this->assertFalse($range->isInPast());
    }



    public function testOverlapsRange()
    {
        $base_range = new DateRange(new DateTime('2020-08-10'), new DateTime('2020-08-20'));

        $overlaping = new DateRange(new DateTime('2020-08-05'), new DateTime('2020-08-09'));
        $this->assertFalse($overlaping->overlapsRange($base_range));

        $overlaping->setTo(new DateTime('2020-08-10'));
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping->setTo(new DateTime('2020-08-12'));
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping->setFrom(new DateTime('2020-08-10'));
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping->setFrom(new DateTime('2020-08-11'));
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping->setTo(new DateTime('2020-08-20'));
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping->setTo(new DateTime('2020-08-21'));
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping->setTo(new DateTime('2020-08-25'));
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping->setFrom(new DateTime('2020-08-20'));
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping->setFrom(new DateTime('2020-08-21'));
        $this->assertFalse($overlaping->overlapsRange($base_range));
    }



    public function testGetFormattedGetters()
    {
        $range = new DateRange(new DateTime('2021-01-01'), new DateTime('2021-12-31'));
        $this->assertEquals('1.1.2021', $range->getFrom('j.n.Y'));
        $this->assertEquals('31.12.2021', $range->getTo('j.n.Y'));
    }



    public function testGetQuarters() : void
    {
        $full_year = new DateRange('2021-01-01 15:30', '2021-12-31 02:00');
        $quarters = $full_year->getQuarters();

        $this->assertCount(4, $quarters);
        $this->assertEquals('2021-01-01 00:00:00 - 2021-03-31 23:59:59', $quarters[0]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-04-01 00:00:00 - 2021-06-30 23:59:59', $quarters[1]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-07-01 00:00:00 - 2021-09-30 23:59:59', $quarters[2]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-10-01 00:00:00 - 2021-12-31 23:59:59', $quarters[3]->format('Y-m-d H:i:s'));

        $three_years = new DateRange('2020-01-01', '2022-12-31');
        $quarters = $three_years->getQuarters();

        $this->assertCount(12, $quarters);
        $this->assertEquals('2020-01-01 - 2020-03-31', $quarters[0]->format('Y-m-d'));
        $this->assertEquals('2020-10-01 - 2020-12-31', $quarters[3]->format('Y-m-d'));
        $this->assertEquals('2021-07-01 - 2021-09-30', $quarters[6]->format('Y-m-d'));
        $this->assertEquals('2022-10-01 - 2022-12-31', $quarters[11]->format('Y-m-d'));

        $two_full_quarters = new DateRange('2024-04-01', '2024-09-30');
        $quarters = $two_full_quarters->getQuarters();

        $this->assertCount(2, $quarters);
        $this->assertEquals('2024-04-01 - 2024-06-30', $quarters[0]->format('Y-m-d'));
        $this->assertEquals('2024-07-01 - 2024-09-30', $quarters[1]->format('Y-m-d'));

        $sub_one_quarter = new DateRange('2024-05-05', '2024-06-02');
        $quarters = $sub_one_quarter->getQuarters();

        $this->assertCount(1, $quarters);
        $this->assertEquals('2024-05-05 00:00:00 - 2024-06-02 23:59:59', $quarters[0]->format('Y-m-d H:i:s'));

        $multiple_quarters_non_aligned = new DateRange('2024-02-08', '2025-01-01');
        $quarters = $multiple_quarters_non_aligned->getQuarters();

        $this->assertCount(5, $quarters);
        $this->assertEquals('2024-02-08 - 2024-03-31', $quarters[0]->format('Y-m-d'));
        $this->assertEquals('2024-04-01 - 2024-06-30', $quarters[1]->format('Y-m-d'));
        $this->assertEquals('2024-07-01 - 2024-09-30', $quarters[2]->format('Y-m-d'));
        $this->assertEquals('2024-10-01 - 2024-12-31', $quarters[3]->format('Y-m-d'));
        $this->assertEquals('2025-01-01 - 2025-01-01', $quarters[4]->format('Y-m-d'));
    }



    public function testGetHalfYears() : void
    {
        $full_year = new DateRange('2021-01-01 15:30', '2021-12-31 02:00');
        $halves = $full_year->getHalfYears();

        $this->assertCount(2, $halves);
        $this->assertEquals('2021-01-01 00:00:00 - 2021-05-31 23:59:59', $halves[0]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-06-01 00:00:00 - 2021-12-31 23:59:59', $halves[1]->format('Y-m-d H:i:s'));

        $three_years = new DateRange('2020-01-01', '2022-12-31');
        $halves = $three_years->getHalfYears();

        $this->assertCount(6, $halves);
        $this->assertEquals('2020-01-01 - 2020-05-31', $halves[0]->format('Y-m-d'));
        $this->assertEquals('2021-06-01 - 2021-12-31', $halves[3]->format('Y-m-d'));
        $this->assertEquals('2022-06-01 - 2022-12-31', $halves[5]->format('Y-m-d'));

        $sub_one_half = new DateRange('2024-04-05', '2024-05-02');
        $halves = $sub_one_half->getHalfYears();

        $this->assertCount(1, $halves);
        $this->assertEquals('2024-04-05 00:00:00 - 2024-05-02 23:59:59', $halves[0]->format('Y-m-d H:i:s'));

        $multiple_halves_non_aligned = new DateRange('2024-02-08', '2025-01-01');
        $halves = $multiple_halves_non_aligned->getHalfYears();

        $this->assertCount(3, $halves);
        $this->assertEquals('2024-02-08 - 2024-05-31', $halves[0]->format('Y-m-d'));
        $this->assertEquals('2024-06-01 - 2024-12-31', $halves[1]->format('Y-m-d'));
        $this->assertEquals('2025-01-01 - 2025-01-01', $halves[2]->format('Y-m-d'));
    }



    public function testGetYears() : void
    {
        $full_year = new DateRange('2021-01-01 15:30', '2021-12-31 02:00');
        $years = $full_year->getYears();

        $this->assertCount(1, $years);
        $this->assertEquals('2021-01-01 00:00:00 - 2021-12-31 23:59:59', $years[0]->format('Y-m-d H:i:s'));

        $three_years = new DateRange('2020-01-01', '2022-12-31');
        $years = $three_years->getYears();

        $this->assertCount(3, $years);
        $this->assertEquals('2020-01-01 - 2020-12-31', $years[0]->format('Y-m-d'));
        $this->assertEquals('2021-01-01 - 2021-12-31', $years[1]->format('Y-m-d'));
        $this->assertEquals('2022-01-01 - 2022-12-31', $years[2]->format('Y-m-d'));

        $sub_one = new DateRange('2024-05-05', '2024-06-02');
        $years = $sub_one->getYears();

        $this->assertCount(1, $years);
        $this->assertEquals('2024-05-05 00:00:00 - 2024-06-02 23:59:59', $years[0]->format('Y-m-d H:i:s'));

        $multiple_years_not_aligned = new DateRange('2024-02-08', '2025-01-01');
        $years = $multiple_years_not_aligned->getYears();

        $this->assertCount(2, $years);
        $this->assertEquals('2024-02-08 - 2024-12-31', $years[0]->format('Y-m-d'));
        $this->assertEquals('2025-01-01 - 2025-01-01', $years[1]->format('Y-m-d'));
    }



    public function testGetMonths() : void
    {
        $aligned = new DateRange('2021-01-01 15:30', '2021-03-31 02:00');
        $months = $aligned->getMonths();

        $this->assertCount(3, $months);
        $this->assertEquals('2021-01-01 00:00:00 - 2021-01-31 23:59:59', $months[0]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-02-01 00:00:00 - 2021-02-28 23:59:59', $months[1]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-03-01 00:00:00 - 2021-03-31 23:59:59', $months[2]->format('Y-m-d H:i:s'));

        $single = new DateRange('2020-01-01', '2020-01-31');
        $months = $single->getMonths();

        $this->assertCount(1, $months);
        $this->assertEquals('2020-01-01 - 2020-01-31', $months[0]->format('Y-m-d'));

        $sub_one = new DateRange('2024-05-05', '2024-05-12');
        $months = $sub_one->getMonths();

        $this->assertCount(1, $months);
        $this->assertEquals('2024-05-05 00:00:00 - 2024-05-12 23:59:59', $months[0]->format('Y-m-d H:i:s'));

        $multiple_not_aligned = new DateRange('2024-02-08', '2024-04-15');
        $months = $multiple_not_aligned->getMonths();

        $this->assertCount(3, $months);
        $this->assertEquals('2024-02-08 - 2024-02-29', $months[0]->format('Y-m-d'));
        $this->assertEquals('2024-03-01 - 2024-03-31', $months[1]->format('Y-m-d'));
        $this->assertEquals('2024-04-01 - 2024-04-15', $months[2]->format('Y-m-d'));
    }



    public function testCutWithRange() : void
    {
        $main_range = new DateRange('2024-05-16 12:59', '2024-09-28 16:30');

        $outside_range_1 = new DateRange('2021-01-01', '2022-03-12');
        $this->assertEquals([$main_range], $main_range->cutWithRange($outside_range_1));

        $outside_range_2 = new DateRange('2025-01-01', '2025-03-12');
        $this->assertEquals([$main_range], $main_range->cutWithRange($outside_range_2));

        $partial_left = new DateRange('2023-12-15', '2024-06-07 13:55');
        $result = $main_range->cutWithRange($partial_left);
        $this->assertEquals('2024-05-16 00:00:00 - 2024-06-07 23:59:59', $result[0]->format('Y-m-d H:i:s'));
        $this->assertEquals('2024-06-08 00:00:00 - 2024-09-28 23:59:59', $result[1]->format('Y-m-d H:i:s'));

        $partial_right = new DateRange('2024-07-15', '2024-12-11 13:55');
        $result = $main_range->cutWithRange($partial_right);
        $this->assertEquals('2024-05-16 - 2024-07-14', $result[0]->format('Y-m-d'));
        $this->assertEquals('2024-07-15 - 2024-09-28', $result[1]->format('Y-m-d'));

        $sandwich = new DateRange('2024-06-01', '2024-08-11 13:55');
        $result = $main_range->cutWithRange($sandwich);
        $this->assertEquals('2024-05-16 - 2024-05-31', $result[0]->format('Y-m-d'));
        $this->assertEquals('2024-06-01 - 2024-08-11', $result[1]->format('Y-m-d'));
        $this->assertEquals('2024-08-12 - 2024-09-28', $result[2]->format('Y-m-d'));
    }



    public function testSubstract() : void
    {
        $main_range = new DateRange('2024-02-12', '2024-08-16');

        $left = new DateRange('2024-01-01', '2024-03-15');
        $this->assertEquals('2024-03-16 - 2024-08-16', $main_range->substract($left)[0]->format('Y-m-d'));

        $right = new DateRange('2024-05-01', '2024-09-15');
        $this->assertEquals('2024-02-12 - 2024-04-30', $main_range->substract($right)[0]->format('Y-m-d'));

        $middle = new DateRange('2024-04-01', '2024-06-15');
        $res = $main_range->substract($middle);

        $this->assertCount(2, $res);
        $this->assertEquals('2024-02-12 - 2024-03-31', $res[0]->format('Y-m-d'));
        $this->assertEquals('2024-06-16 - 2024-08-16', $res[1]->format('Y-m-d'));

        $outside = new DateRange('2020-01-15', '2022-08-30');
        $this->assertEquals([$main_range], $main_range->substract($outside));

        $outside = new DateRange('2025-01-15', '2026-08-30');
        $this->assertEquals([$main_range], $main_range->substract($outside));

        $full = new DateRange('2020-01-15', '2026-08-30');
        $this->assertEquals([], $main_range->substract($full));
    }
}
