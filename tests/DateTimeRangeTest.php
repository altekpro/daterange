<?php

namespace Altekpro\DateRange;

use PHPUnit\Framework\TestCase;
use DateTime;

class DateTimeRangeTest extends TestCase
{
    public function testCannotCreateForBackwardsDates()
    {
        $from = new DateTime('2020-06-15');
        $to = new DateTime('2020-06-14');

        $this->expectException(\InvalidArgumentException::class);

        new DateTimeRange($from, $to);
    }



    public function testCreateForYear()
    {
        $range = DateTimeRange::createForYear(2020);
        $this->assertEquals(['2020-01-01 00:00:00', '2020-12-31 23:59:59'],
            [$range->getFrom()->format('Y-m-d H:i:s'), $range->getTo()->format('Y-m-d H:i:s')]);
    }



    public function testRangeIsInRange()
    {
        $outer = new DateTimeRange(new DateTime('2020-01-01 12:45'), new DateTime('2020-01-01 16:34'));

        // inside range - closed interval
        $inner = new DateTimeRange(new DateTime('2020-01-01 13:17'), new DateTime('2020-01-01 16:33'));
        $this->assertTrue($inner->isInRange($outer));

        // inside range - right open interval
        $inner = new DateTimeRange(new DateTime('2020-01-01 13:17'), new DateTime('2020-01-01 16:34'));
        $this->assertTrue($inner->isInRange($outer));

        // inside range - left open interval
        $inner = new DateTimeRange(new DateTime('2020-01-01 12:45'), new DateTime('2020-01-01 16:33'));
        $this->assertTrue($inner->isInRange($outer));

        // inside range - open interval
        $inner = new DateTimeRange(new DateTime('2020-01-01 12:45'), new DateTime('2020-01-01 16:34'));
        $this->assertTrue($inner->isInRange($outer));

        // left is outside of range
        $inner = new DateTimeRange(new DateTime('2020-01-01 10:00'), new DateTime('2020-01-01 16:34'));
        $this->assertFalse($inner->isInRange($outer));

        // right is outside of range
        $inner = new DateTimeRange(new DateTime('2020-01-01 12:45'), new DateTime('2020-01-02 12:00'));
        $this->assertFalse($inner->isInRange($outer));

        // entire interval is outside, use DateRange as outer
        $outer = new DateRange(new DateTime('2020-01-01'), new DateTime('2020-01-01'));
        $inner = new DateTimeRange(new DateTime('2020-01-03'), new DateTime('2020-01-04'));
        $this->assertFalse($inner->isInRange($outer));

        // fail if argument is not range
        $this->expectException(\InvalidArgumentException::class);
        $inner = new DateTimeRange(new DateTime('2020-01-01'), new DateTime('2020-01-02'));
        $inner->isInRange(new \StdClass);
    }



    public function testGetQuarters() : void
    {
        $full_year = new DateTimeRange('2021-01-01 15:30', '2021-12-31 02:00');
        $quarters = $full_year->getQuarters();

        $this->assertCount(4, $quarters);
        $this->assertEquals('2021-01-01 15:30:00 - 2021-03-31 23:59:59', $quarters[0]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-04-01 00:00:00 - 2021-06-30 23:59:59', $quarters[1]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-07-01 00:00:00 - 2021-09-30 23:59:59', $quarters[2]->format('Y-m-d H:i:s'));
        $this->assertEquals('2021-10-01 00:00:00 - 2021-12-31 02:00:00', $quarters[3]->format('Y-m-d H:i:s'));

        $sub_one_quarter = new DateTimeRange('2024-05-05 14:20:56', '2024-06-02 16:30:24');
        $quarters = $sub_one_quarter->getQuarters();

        $this->assertCount(1, $quarters);
        $this->assertEquals('2024-05-05 14:20:56 - 2024-06-02 16:30:24', $quarters[0]->format('Y-m-d H:i:s'));
    }
}
