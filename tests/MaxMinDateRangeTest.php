<?php

use Altekpro\DateRange\MaxMinDateRange;
use PHPUnit\Framework\TestCase;

class MaxMinDateRangeTest extends TestCase
{

    public function testConstruct()
    {
        $range = new MaxMinDateRange(new DateTime('1970-01-01'), new DateTime('2100-01-01'));
        $this->assertInstanceOf(DateTime::class, $range->getFrom());
        $this->assertInstanceOf(DateTime::class, $range->getTo());

        $range = new MaxMinDateRange(null, new DateTime('2100-01-01'));
        $this->assertEquals(null, $range->getFrom());
        $this->assertInstanceOf(DateTime::class, $range->getTo());

        $range = new MaxMinDateRange(new DateTime('2100-01-01'), null);
        $this->assertInstanceOf(DateTime::class, $range->getFrom());
        $this->assertEquals(null, $range->getTo());

        $range = new MaxMinDateRange('1970-01-01', '2100-01-01');
        $this->assertInstanceOf(DateTime::class, $range->getFrom());
        $this->assertInstanceOf(DateTime::class, $range->getTo());

        $range = new MaxMinDateRange(null, '2100-01-01');
        $this->assertEquals(null, $range->getFrom());
        $this->assertInstanceOf(DateTime::class, $range->getTo());

        $range = new MaxMinDateRange(null, null);
        $this->assertEquals(null, $range->getFrom());
        $this->assertEquals(null, $range->getTo());


        $this->expectException(\InvalidArgumentException::class);
        new MaxMinDateRange('21saa00-0101', null);
        new MaxMinDateRange(null, '21saa00-0101');
        new MaxMinDateRange('21saa00-0101', '21s...?$@aa00-0101');
        new MaxMinDateRange(new DateTime('2100-01-01'), '21s...?$@aa00-0101');

    }

    public function testCannotCreateForBackwardsDates()
    {
        $from = new DateTime('2020-06-15');
        $to = new DateTime('2020-06-14');

        $this->expectException(\InvalidArgumentException::class);

        new MaxMinDateRange($from, $to);
    }

    public function testCreateForYear()
    {
        $range = MaxMinDateRange::createForYear(2020);
        $this->assertEquals(['2020-01-01', '2020-12-31'], [$range->getFrom()->format('Y-m-d'), $range->getTo()->format('Y-m-d')]);

        $range = MaxMinDateRange::createForYear(2022);
        $this->assertEquals(['2022-01-01', '2022-12-31'], [$range->getFrom()->format('Y-m-d'), $range->getTo()->format('Y-m-d')]);

        $this->expectException(\RuntimeException::class);
        MaxMinDateRange::createForYear(45);

        $this->expectException(\RuntimeException::class);
        MaxMinDateRange::createForYear(250);
    }

    public function testRangeIsInRange()
    {
        $from = new DateTime('2020-01-02');
        $to = new DateTime('2020-01-03');

        $range = new MaxMinDateRange($from, $to);
        $result = $range->isInRange(
                        new MaxMinDateRange(
                                (clone $from)->modify('-1 day'),
                                (clone $to)->modify('+1 day')
                        )
                    );

        $this->assertTrue($result);

        $range = new MaxMinDateRange($from, $to);
        $result = $range->isInRange(
            new MaxMinDateRange(
                (clone $from)->modify('-1 day'),
                null
            )
        );

        $this->assertTrue($result);

        $range = new MaxMinDateRange($from, $to);
        $result = $range->isInRange(
            new MaxMinDateRange(
                null,
                (clone $to)->modify('+1 day')
            )
        );

        $this->assertTrue($result);


        $range = new MaxMinDateRange($from, $to);
        $result = $range->isInRange(
            new MaxMinDateRange(
                null,
                null
            )
        );

        $this->assertTrue($result);


        $range = new MaxMinDateRange(null, $to);
        $result = $range->isInRange(
            new MaxMinDateRange(
                (clone $from)->modify('-1 day'),
                (clone $to)->modify('+1 day')
            )
        );

        $this->assertFalse($result);


        $range = new MaxMinDateRange($from, null);
        $result = $range->isInRange(
            new MaxMinDateRange(
                (clone $from)->modify('-1 day'),
                (clone $to)->modify('+1 day')
            )
        );

        $this->assertFalse($result);


        $range = new MaxMinDateRange(null, null);
        $result = $range->isInRange(
            new MaxMinDateRange(
                (clone $from)->modify('-1 day'),
                (clone $to)->modify('+1 day')
            )
        );

        $this->assertFalse($result);

        $range = new MaxMinDateRange(null, $to);
        $result = $range->isInRange(
            new MaxMinDateRange(
                null,
                (clone $to)->modify('+1 day')
            )
        );

        $this->assertTrue($result);

        $range = new MaxMinDateRange($from, null);
        $result = $range->isInRange(
            new MaxMinDateRange(
                (clone $from)->modify('-1 day'),
                null
            )
        );

        $this->assertTrue($result);

        $range = new MaxMinDateRange(null, null);
        $result = $range->isInRange(
            new MaxMinDateRange(null, null)
        );

        $this->assertTrue($result);


        $this->expectException(\InvalidArgumentException::class);
        $range = new MaxMinDateRange($from, $to);
        $range->isInRange(new \StdClass);

    }

    public function testIsCurrent()
    {
        $from = (new DateTime())->modify('-1 day');
        $to = (new DateTime())->modify('+1 day');

        $range = new MaxMinDateRange($from, $to);
        $result = $range->isCurrent();
        $this->assertTrue($result);

        $range = new MaxMinDateRange($from, null);
        $result = $range->isCurrent();
        $this->assertTrue($result);

        $range = new MaxMinDateRange(null, null);
        $result = $range->isCurrent();
        $this->assertTrue($result);

        $from = (new DateTime())->modify('+1 day');
        $to = (new DateTime())->modify('+2 day');

        $range = new MaxMinDateRange($from, null);
        $result = $range->isCurrent();
        $this->assertFalse($result);
    }

    public function testGetYear()
    {
        $range = new MaxMinDateRange(new DateTime('2020-01-01'), new DateTime('2020-12-31'));
        $this->assertEquals(2020, $range->getYear());

        $this->expectException(\RuntimeException::class);
        $range = new MaxMinDateRange(new DateTime('2019-06-01'), new DateTime('2020-06-31'));
        $range->getYear();

        $range = new MaxMinDateRange(new DateTime('2020-01-01'), null);
        $range->getYear();

        $range = new MaxMinDateRange(null, null);
        $range->getYear();

        $range = new MaxMinDateRange(null, new DateTime('2020-01-01'));
        $range->getYear();
    }


    public function testIsInPast()
    {
        $range = new MaxMinDateRange(new DateTime('2020-01-01'), new DateTime('2020-12-31'));
        $this->assertTrue($range->isInPast());

        $range = new MaxMinDateRange(new DateTime('2020-01-01'), null);
        $this->assertFalse($range->isInPast());

        $range = new MaxMinDateRange(null, new DateTime('2020-12-31'));
        $this->assertTrue($range->isInPast());

        $range = new MaxMinDateRange(null, null);
        $this->assertFalse($range->isInPast());
    }


    public function testOverlapsRange()
    {
        $from = new DateTime('2020-08-10');
        $to = new DateTime('2020-08-20');

        $o_from = new DateTime('2020-08-5');
        $o_to = new DateTime('2020-08-9');

        $base_range = new MaxMinDateRange($from, $to);

        $overlaping = new MaxMinDateRange($from, $to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($from, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, $to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($o_from, $o_to);
        $this->assertFalse($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($o_from, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, $o_to);
        $this->assertFalse($overlaping->overlapsRange($base_range));


        $base_range = new MaxMinDateRange(null, $to);

        $overlaping = new MaxMinDateRange($from, $to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($from, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, $to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($o_from, $o_to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($o_from, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, $o_to);
        $this->assertTrue($overlaping->overlapsRange($base_range));



        $base_range = new MaxMinDateRange($from, null);

        $overlaping = new MaxMinDateRange($from, $to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($from, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, $to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($o_from, $o_to);
        $this->assertFalse($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($o_from, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, $o_to);
        $this->assertFalse($overlaping->overlapsRange($base_range));



        $base_range = new MaxMinDateRange(null, null);

        $overlaping = new MaxMinDateRange($from, $to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($from, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, $to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));


        $overlaping = new MaxMinDateRange($o_from, $o_to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange($o_from, null);
        $this->assertTrue($overlaping->overlapsRange($base_range));

        $overlaping = new MaxMinDateRange(null, $o_to);
        $this->assertTrue($overlaping->overlapsRange($base_range));

    }

    public function testGetters()
    {
        $from = new DateTime('2020-01-01 00:00:00');
        $to = new DateTime('2020-01-03 23:59:59');

        $range = new MaxMinDateRange($from, $to);
        $this->assertEquals($from, $range->getFrom());
        $this->assertEquals($from->format('d.m.Y'), $range->getFrom('d.m.Y'));
        $this->assertEquals($to, $range->getTo());
        $this->assertEquals($to->format('d.m.Y'), $range->getTo('d.m.Y'));

        $range = new MaxMinDateRange(null, null);
        $this->assertEquals(null, $range->getFrom());
        $this->assertEquals(null, $range->getFrom('d.m.Y'));
        $this->assertEquals(null, $range->getTo());
        $this->assertEquals(null, $range->getTo('d.m.Y'));

    }

    public function testIsNull()
    {
        $from = new DateTime('2020-01-01 00:00:00');
        $to = new DateTime('2020-01-03 23:59:59');

        $range = new MaxMinDateRange($from, $to);
        $this->assertFalse($range->isNull());

        $range = new MaxMinDateRange($from, null);
        $this->assertFalse($range->isNull());

        $range = new MaxMinDateRange(null, $to);
        $this->assertFalse($range->isNull());

        $range = new MaxMinDateRange(null, null);
        $this->assertTrue($range->isNull());
    }

}
